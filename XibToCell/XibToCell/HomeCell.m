//
//  HomeCell.m
//  XibToCell
//
//  Created by mac on 2018/3/15.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "HomeCell.h"

@implementation HomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    NSArray *xibArray = [[NSBundle mainBundle] loadNibNamed:@"MoreView" owner:self options:nil];
    MoreView *view1 = xibArray.firstObject;
    view1.frame = CGRectMake(0, 0, CGRectGetWidth(self.moreView.frame), CGRectGetHeight(self.moreView.frame));
    [self.moreView addSubview:view1];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
