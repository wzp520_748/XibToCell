//
//  HomeCell.h
//  XibToCell
//
//  Created by mac on 2018/3/15.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoreView.h"

@interface HomeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet MoreView *moreView;

@end
